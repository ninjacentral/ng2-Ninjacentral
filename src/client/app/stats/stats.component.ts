import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-about',
  templateUrl: 'stats.component.html',
  styleUrls: ['stats.component.css']
})
export class StatsComponent { }
